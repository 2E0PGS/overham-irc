### What is Overham_IRC? ###
Overham_IRC is a IRC bot which queries Overham: https://bitbucket.org/2E0EOL/overham-server

### How do I get set up? ###

* Clone Repo
* apt get install node.js or install .exe
* npm install (Dependencies) irc, multiline, restify
* node Overham_IRC.js or Start_Overham_IRC.bat

### Who do I talk to? ###

* Peter Stevenson (2E0PGS)

### Licence? ###

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```