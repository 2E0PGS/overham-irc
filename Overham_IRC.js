/*
Overham_IRC A IRC bot for querying Overham.

Copyright (C) <2017>  <Peter Stevenson (2E0PGS)>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var irc = require('irc'); // Core IRC library.
var multiLine = require('multiline'); // Used so we can have nice multiLine /help responce.
var restify = require('restify'); // Core REST client Library.

var serverUrl = "http://api.overham.overchat.org";
var ircChannel = "#NBARC"; // Set IRC channel here
var restServerReply = "NULL"; // Store REST JSON extracted object
var command; // Store command prefix.
var suffix; // Store command suffix.
var serverVersion = 2; // Store REST API Endpoint Version.

var ircClient = new irc.Client('chat.freenode.net', 'Overham_IRC', { // Join NBARC IRC channel
	channels: [(ircChannel)], realName: '2E0PGS Overham_IRC Bot', port: 6697, autoRejoin: true, autoConnect: true, debug: false, secure: true, floodProtection: true, floodProtectionDelay: 500,
});

ircClient.addListener('message', function (from, to, message) {
	if (message[0] === '$') {
		console.log(from + ' => ' + to + ': ' + message);
		command = message.toLowerCase().split(" ")[0].substring(1);
		if (command === "help") { // Here is our command list. Add commands here.
			ircClient.say((ircChannel), (help));
		}
		if (command === "about") {
			ircClient.say((ircChannel), (about));
		}
		if (command === "commands") {
			ircClient.say((ircChannel), (help));
		}
		if (command === "serverurl") {
			ircClient.say((ircChannel), ("ServerURL: " + serverUrl + ", Rest API Endpoint Version: " + serverVersion));
		}
		if (command === "ping") {
			ircClient.say((ircChannel), "Pong!");
		}
		if (command === "callsign") {
			suffix = message.toLowerCase().substring(command.length + 2);
			if (suffix.match(/^[^A-Z0-9]*$/i)) { // User input validation
				console.log("* User input failed validation.")
				ircClient.say((ircChannel), "* Error invalid input detected.");
			}
			else {
				runRestClient();
			}
		}
		if (command === "serveruptime") {
			runRestClient();
		}
		if (command === "resistor") {
			suffix = message.toLowerCase().substring(command.length + 2);
			if (suffix.match(/^[^A-Z0-9]*$/i)) { // User input validation
				console.log("* User input failed validation.")
				ircClient.say((ircChannel), "* Error invalid input detected.");
			}
			else {
				runRestClient();
			}
		}
		if (command === "qcode") {
			suffix = message.toLowerCase().substring(command.length + 2);
			if (suffix.match(/^[^A-Z0-9]*$/i)) { // User input validation
				console.log("* User input failed validation.")
				ircClient.say((ircChannel), "* Error invalid input detected.");
			}
			else {
				runRestClient();
			}
		}
	}
});

ircClient.addListener('error', function (message) { // Log errors to prevent fatal crash.
	console.log('error: ', message);
	process.exit(1); // Exit node.js with an error
});

//---------------------REST Client---------------------
function runRestClient() {
	var restClient = restify.createJsonClient({
		url: (serverUrl),
	});

	if (command === "callsign") { // Callsign REST request
		restClient.get(('/v' + serverVersion + '/callsign/info?callsign=' + suffix), function (err, req, res, obj) {
			console.log('Server returned: %j', obj);
			if (obj.data != undefined && obj.data.amateurClass != undefined) {
				restServerReply = "Callsign: " + obj.data.value + ", Type: " + obj.data.type + ", Country: " + obj.data.country + ", Class: " + obj.data.amateurClass.string + ", Region: " + obj.data.region;
				sendRestReplytoIRC();
			}
		});
	}

	if (command === "serveruptime") { // Serveruptime REST request
		restClient.get('/v' + serverVersion + '/system/uptime', function (err, req, res, obj) {
			console.log('Server returned: %j', obj);
			restServerReply = "Seconds: " + obj.data.seconds;
			sendRestReplytoIRC();
		});
	}

	if (command === "resistor") { // Resistor REST request
		restClient.get(('/v' + serverVersion + '/resistor/encode?ohms=' + suffix), function (err, req, res, obj) {
			console.log('Server returned: %j', obj);
			if (obj.data != undefined && obj.data.band0str != undefined) {
				restServerReply = "band0: " + obj.data.band0str + ", band1: " + obj.data.band1str + ", band2: " + obj.data.band2str;
				sendRestReplytoIRC();
			}
		});
	}

	if (command === "qcode") { // Qcode REST request
		restClient.get(('/v' + serverVersion + '/qcode/lookup?code=' + suffix), function (err, req, res, obj) {
			console.log('Server returned: %j', obj);
			if (obj.data != undefined && obj.data.tx != undefined) {
				restServerReply = "TX: " + obj.data.tx + ", RX: " + obj.data.rx + ", Authority: " + obj.data.authority;
				sendRestReplytoIRC();
			}
		});
	}
};
//---------------------REST Client------------------END

// Send the message to IRC server in a separate function for code efficency.
function sendRestReplytoIRC() {
	console.log(restServerReply);
	ircClient.say((ircChannel), (restServerReply));
}

// Command strings
var help = multiLine(function () {/*
###################################################################
Help function:

$ping Checks if bot is online.

$help Lists avalible commands.

$about Prints about message.

$serverurl Prints the Overham backend API server URL and version.

$serveruptime Prints the server uptime in seconds.

$callsign <callsign> Prints info about specified callsign.

$resistor <ohms> Prints band colours of specified resistor value.

$qcode <qcode> Prints definition of specified qcode.
###################################################################
*/});

// About string
var about = multiLine(function () {/*
###################################################################
Overham_IRC A IRC bot for querying Overham.
Repo for bot: https://bitbucket.org/2E0PGS/overham_irc
Repo for OverhamJ: https://bitbucket.org/2E0EOL/overhamj
Owner: @2E0PGS
Written by: @2E0PGS
###################################################################
*/});
